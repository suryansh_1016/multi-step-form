const step1 = document.querySelector(".personal-information");
const step2 = document.querySelector(".select-your-plan");
const step3 = document.querySelector(".pick-addons");
const step4 = document.querySelector(".billing");
const step5 = document.querySelector(".end");

const stepNo1 = document.getElementById("step-1");
stepNo1.classList.add("active-step");
const stepNo2 = document.getElementById("step-2");
const stepNo3 = document.getElementById("step-3");
const stepNo4 = document.getElementById("step-4");
const cards = document.querySelectorAll(".plan");

const chkbox = document.querySelector(".toggle-checkbox");


// const nameRegex = /^[a-zA-Z'-]+(?:\s[a-zA-Z'-]+)*$/;
// const emailRegex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
// const phoneRegex = /^\+?[0-9]{1,3}?[-. ]?\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/;

function validateInput(input, type) {
    const validations = {
        name: /^[a-zA-Z'-]+(?:\s[a-zA-Z'-]+)*$/,
        email: /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/,
        phone: /^\+?[0-9]{1,3}?[-. ]?\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/,
    };

    const regex = validations[type];
    if (!regex) {
        throw new Error(`Invalid validation type: ${type}`);
    }
    return regex.test(input);
}

let form = document.querySelector("form");
let nameInput = document.querySelector("#nameInput");
let emailInput = document.querySelector("#emailInput");
let phoneInput = document.querySelector("#phoneInput");
let nextStep = document.querySelector("#next-step");


nextStep.addEventListener("click", function (event) {
    event.preventDefault();

    const name = nameInput.value.trim();
    const email = emailInput.value.trim();
    const phone = phoneInput.value.trim();

    const isValidName = validateInput(name, "name");
    const isValidEmail = validateInput(email, "email");
    const isValidPhone = validateInput(phone, "phone");

    if (!isValidName) {
        document.querySelector(".name-error").style.display = "block";
        nameInput.style.borderColor = "red";
    } else {
        document.querySelector(".name-error").style.display = "none";
        nameInput.style.borderColor = "";
    }

    if (!isValidEmail) {
        document.querySelector(".email-error").style.display = "block";
        emailInput.style.borderColor = "red";
    } else {
        document.querySelector(".email-error").style.display = "none";
        emailInput.style.borderColor = "";
    }

    if (!isValidPhone) {
        document.querySelector(".phone-error").style.display = "block";
        phoneInput.style.borderColor = "red";
    } else {
        document.querySelector(".phone-error").style.display = "none";
        phoneInput.style.borderColor = "";
    }

    if (isValidName && isValidEmail && isValidPhone) {
        stepNo1.classList.remove("active-step");
        stepNo2.classList.add("active-step");
        step1.style.display = "none";
        step2.style.display = "flex";
    }
});

let bill = []

let monthlyPrices = [9, 12, 15];
let yearlyPrices = [90, 120, 150];

let monthlyAddon = [1, 2, 2];
let yearlyAddon = [10, 20, 20];

function isMonthly() {
    const chkbox = document.querySelector(".toggle-checkbox");
    return !chkbox.checked;
}

let planSelected = cards[0];
planSelected.classList.add("selected-plan");

cards.forEach((card, index) => {
    card.addEventListener("click", function () {
        planSelected.classList.remove("selected-plan");
        card.classList.add("selected-plan");
        planSelected = card;
    })
})

let toggleCheckbox = document.querySelector(".toggle-checkbox");
toggleCheckbox.addEventListener("click", function () {
    if (!isMonthly()) {
        document.querySelector(".monthly").style.color = "var(--Cool-gray)";
        document.querySelector(".yearly").style.color = "var(--Marine-blue)";

    } else {
        document.querySelector(".monthly").style.color = "var(--Marine-blue)";
        document.querySelector(".yearly").style.color = "var(--Cool-gray)";
    }

})


function renderPlans() {
    cards.forEach((card, index) => {

        const prices = card.querySelector("p");
        const freeDiv = card.querySelector(".free");

        if (!isMonthly()) {
            prices.textContent = `$${yearlyPrices[index]}/yr`;

            if (!freeDiv) {
                let free = document.createElement("div");
                free.className = "free";
                free.innerText = "2 months free";
                free.style.padding = "2px 15px";
                free.style.fontSize = "16px";
                free.style.color = "var(--Marine-blue)";
                card.appendChild(free);
            }
        } else {
            prices.textContent = `$${monthlyPrices[index]}/mo`;

            if (freeDiv) {
                freeDiv.remove();
            }
        }
    });
}

chkbox.addEventListener("change", function () {
    renderPlans();
});

const addOns = {
    "Online service": {
        monthly: 1,
        yearly: 10
    },
    "Larger Storage": {
        monthly: 2,
        yearly: 20
    },
    "Customizable profile": {
        monthly: 2,
        yearly: 20
    },
}

let addOnsPrice = [];

const adds = document.querySelectorAll(".add");
adds.forEach(add => {
    const checkbox = add.querySelector(".addcheck");

    checkbox.addEventListener("change", function () {
        if (checkbox.checked) {
            add.style.backgroundColor = "var(--Magnolia)";
            add.style.border = "2px solid var(--Purplish-blue)";
        } else {
            add.style.backgroundColor = "";
            add.style.border = "";
        }
    });
});


function addBill() {
    const billdata = document.querySelector(".billdata");
    billdata.querySelector("h4").innerText = `${bill[0]}(${bill[2]})`
    if (isMonthly()) {
        billdata.querySelector("div").innerText = `$${bill[1]}/mo`;
    }
    else {
        billdata.querySelector("div").innerText = `$${bill[1]}/yr`;
    }
}


function addSubBills() {
    let subbill = document.querySelector(".addOnBill");
    subbill.style.display = "flex";
    subbill.style.flexDirection = "column";

    let sbill = document.createElement("div");
    sbill.className = "subbill";

    sbill.style.display = "flex";
    sbill.style.width = "100%";
    sbill.style.justifyContent = "space-between";
    sbill.style.color = "var(--Cool-gray)";
    sbill.style.fontSize = "16px";
    sbill.style.fontWeight = "500";
    sbill.style.flexDirection = "column";

    if (isMonthly()) {
        for (let i = 0; i < addOnsPrice.length; i += 2) {
            sbill.innerHTML += `<div class="billcontent"><span>${addOnsPrice[i]}</span>
                                <span class="price">+${addOnsPrice[i + 1]}/mo</span> </div>`;
        }
    }
    else {
        for (let i = 0; i < addOnsPrice.length; i += 2) {
            sbill.innerHTML += `<div class="billcontent"><span>${addOnsPrice[i]}</span>
                                <span class="price">+${addOnsPrice[i + 1]}/yr</span> </div>`;
        }
    }
    subbill.innerHTML = sbill.outerHTML;
}

// go back function

function back() {
    const backstep1 = document.getElementById("backstep1");
    backstep1.addEventListener("click", function () {
        step2.style.display = "none";
        step1.style.display = "flex";
        stepNo2.classList.remove("active-step");
        stepNo1.classList.add("active-step")
    })

    const backstep2 = document.getElementById("backstep2");
    backstep2.addEventListener("click", function () {
        step3.style.display = "none";
        step2.style.display = "flex";
        stepNo3.classList.remove("active-step");
        stepNo2.classList.add("active-step")
    })

    const backstep3 = document.getElementById("backstep3");
    backstep3.addEventListener("click", function () {
        step4.style.display = "none";
        step3.style.display = "flex";
        stepNo4.classList.remove("active-step");
        stepNo3.classList.add("active-step")
    })
}
back();

function next() {

    const toStep3 = document.getElementById("to-step3");
    toStep3.addEventListener("click", function () {
        step2.style.display = "none";
        step3.style.display = "flex";
        stepNo3.classList.add("active-step");
        stepNo2.classList.remove("active-step");

        let adprice = document.querySelectorAll("#adprice");
        adprice.forEach((item, index) => {
            if (!isMonthly()) {
                item.textContent = `$${yearlyAddon[index]}/yr`;
            } else {
                item.textContent = `$${monthlyAddon[index]}/mo`;
            }
        })

        bill = [];
        cards.forEach((card, index) => {
            if (card == planSelected) {
                const price = isMonthly() ? monthlyPrices[index] : yearlyPrices[index];
                bill.push(card.querySelector("h4").innerText);
                bill.push(price);
                bill.push(isMonthly() ? 'monthly' : 'yearly');
            }
        })
        console.log(bill);

    })



    const toStep4 = document.getElementById("to-step4");
    toStep4.addEventListener("click", function () {
        step3.style.display = "none";
        step4.style.display = "flex";
        stepNo4.classList.add("active-step");
        stepNo3.classList.remove("active-step");

        addOnsPrice = [];
        const adds = document.querySelectorAll(".add");
        adds.forEach(add => {
            const checkbox = add.querySelector(".addcheck");
            if (checkbox.checked) {
                const addonName = add.querySelector("h4").innerText;
                const addonPrice = isMonthly() ? addOns[addonName].monthly : addOns[addonName].yearly;
                addOnsPrice.push(addonName, addonPrice);
            }
        });

        addBill();
        addSubBills();

        let totalBill = 0;
        for (let i = 0; i < addOnsPrice.length; i += 2) {
            totalBill += addOnsPrice[i + 1];
        }
        totalBill += bill[1];
        if (isMonthly()) {
            document.querySelector(".total-bill-price").innerText = `$${totalBill}/mo`;
            document.querySelector(".total-bill").querySelector("h4").innerText = `Total (per month)`;
        }
        else {
            document.querySelector(".total-bill-price").innerText = `$${totalBill}/yr`;
            document.querySelector(".total-bill").querySelector("h4").innerText = `Total (per year)`;
        }
    });

    const toEnd = document.getElementById("to-end");
    toEnd.addEventListener('click', function () {
        step4.style.display = "none";
        step5.style.display = "flex";
    })

}

next();





